import gulp from 'gulp';
import * as dartSass from 'sass';
import gulpSass from 'gulp-sass';
import browserSync from 'browser-sync';
import autoprefixer from 'gulp-autoprefixer';
import concat from 'gulp-concat';
import imagemin from 'gulp-imagemin';

const sass = gulpSass(dartSass);
const { src, dest, watch, series, parallel } = gulp;

export function html() {
    return gulp.src('src/index.html')
         .pipe(gulp.dest('build'))
         .pipe(browserSync.reload({ stream: true }));
}

function styles() {
    return src('src/scss/main.scss')
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(autoprefixer({
            grid: true,
            overrideBrowserslist: ["last 3 versions"],
            cascade: true
        }))
        .pipe(concat('main.css'))
        .pipe(dest('build/css'))
        .pipe(browserSync.reload({ stream: true }));
}

function images() {
    return src('src/images/*')
        .pipe(imagemin())
        .pipe(dest('build/images'));
}

function reloadPage(done) {
    browserSync.init({
        server: {
            baseDir: 'build/'
        }
    });
    done();
}

function watching() {
    watch(['src/scss/*.scss'], styles);
    watch(['src/*.html'], html);
}

const build = parallel(images, styles, html);
const dev = series(build, parallel(reloadPage, watching));

export { build, dev };
export default parallel(build, dev);